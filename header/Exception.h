#pragma once
#include "types.h"

namespace zifmann::Computer {

#define E_OUT_OF_FRAMES		0x01
#define E_OUT_OF_MEMORY		0x02
#define E_INVALID_ACCESS	0X03

	/// <summary>
	/// Exceptions are system interrupts that occur when something goes out of hand.
	/// </summary>
	class Exception
	{
	public:
		const char* msg;
		const byte type;
	};
}