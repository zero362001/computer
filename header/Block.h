#pragma once
#include "types.h"

namespace zifmann::Computer {
	struct MemoryBlock
	{
		word size;
		word mem;
		MemoryBlock* next;
	};
}
