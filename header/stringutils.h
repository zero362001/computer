#include <string>
#include <sstream>

namespace ziffman::Computer::Utils {

    std::string format(const std::string& fmt, ...);

    inline std::string operator+(const std::string& left, const std::string& right) {
        std::stringstream stream;
        stream << left << right;
        return stream.str();
    }

    inline std::string operator*(const std::string& str, const int& mult) {
        std::stringstream stream;
        for (int i = 0; i < mult; i++)
            stream << str;
        return stream.str();
    }

    inline std::string operator+(const char* left, const std::string& right) {
        return std::string(left) + right;
    }

    inline std::string operator+(const std::string& left, const char* right) {
        return std::string(left) + std::string(right);
    }

    template<typename T>
    inline std::string toString(const T& x) {
        std::stringstream stream;
        stream << x;
        return stream.str();
    }

}
