#include <iostream>
#include <stdlib.h>
#include <stdarg.h>
#include <string>

namespace ziffman::Computer::Utils {
    void print(const std::string& format, ...);
}