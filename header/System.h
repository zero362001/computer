#pragma once

#include "Memory.h"
#include "Processor.h"
#include "Process.h"
#include "Graphics.h"

#include <list>

namespace zifmann::Computer {
	class System
	{
	private:
		Memory* ram_handler;
		Processor* cpu_controller;
		GraphicsController* gfx_controller;

		std::list<Process> processes;

	public:
		System() = default;
		System(const System& other);
		System(System&& other);
		~System();
	};
}
