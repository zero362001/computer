#pragma once
#include "types.h"

namespace zifmann::Computer {

#define BUS_LINE_COUNT 65536

    class Memory;
    class Processor;

    class Bus {
    private:
        byte lines[BUS_LINE_COUNT];
        Memory* memory;
        Processor* processor;
    
    public:
        word ReadLine(word line_no);
        void SetLine(word line_no);
    };
}