#pragma once
#include <unordered_map>
#include <tuple>
#include <list>

#include "types.h"
#include "Process.h"

namespace zifmann::Computer {

	class Process;

// definitions are in number of bits
#define MEMSIZE (1 << (8 * sizeof(word)))
#define FRAMESIZE (1 << 8)
#define ZERO 0x0

	class Memory
	{
	private:
		static const word frame_count = MEMSIZE / FRAMESIZE;
		static const word frame_size = FRAMESIZE;
		static const word page_size = FRAMESIZE;
		byte data[MEMSIZE];
		std::list<word> free_frames;

	public:
		bool initialize();

		bool write(byte data[], word addr, byte n, const Process* process);

		byte read(word addr, const Process* process);

		word get_physical_addr(word logical, const Process* process);

		word allocate_frame();

		word allocate_page(Process* process);

		word allocate_bytes(word bytes, Process* process);

		void delete_page(word page, Process* process);

		void delete_bytes(word addr, byte n, const Process* process);
	};
}
