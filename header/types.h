#pragma once
#include <cstdint>

namespace zifmann::Computer {
	typedef uint8_t byte;
	typedef uint16_t word;
}