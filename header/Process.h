#pragma once
#include "types.h"
#include "Block.h"
#include "Memory.h"

#include <vector>

namespace zifmann::Computer {
	class Memory;
	
	struct Process
	{
		word pid;
		word runtime_1;
		word runtime_2;
		std::vector<word> page_table;
		MemoryBlock* free_blocks_head;
		Memory* mem_handle;

		Process() = default;
		Process(const Process& other);
		Process(Process&& other);
		~Process();
	};
}