#include <string>
#include <vector>

namespace ziffman::Computer::Engine {
    
    class Logger {

    public:
        enum Level {
            DEBUG = 0, INFO = 1, ERROR = 2, WARN = 4
        };

        Logger* GetInstance();

        void Log(Level level, const std::string& message);
        
        void LogWarn(const std::string& message);
        
        void LogError(const std::string& message);
        
        void LogInfo(const std::string& message);
        
        void LogDebug(const std::string& message);

        void WriteLogs();

        ~Logger() {
            WriteLogs();
            delete instance;
        }

    private:
        static Logger* instance;
        std::vector<std::string> logs;
        std::string path;

        Logger();
        
    };

    
}
