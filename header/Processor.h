#pragma once
#include "types.h"

namespace zifmann::Computer {
	class Processor
	{
	private:
		word ac; // Accumulator
		word rx; // X Register
		word ry; // Y Register
		word pc; // Program counter
		byte n : 1; // Negative flag
		byte v : 1; // Overflow flag
		byte r : 1; // Reserved
		byte b : 1; // Break flag
		byte d : 1; // Decimal flag
		byte i : 1; // Interrupt flag
		byte z : 1; // Zero flag
		byte c : 1; // Carry flag
		word sr; // Status register
	
	public:
		void Tick();

		void Fetch();
		void Decode();
		void Execute();

		void WriteToMemory(byte data, word loc);
		byte GetFromMemory(word loc);
	};
}
