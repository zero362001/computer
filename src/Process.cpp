#include "Process.h"

namespace zifmann::Computer {
	Process::Process(const Process& other) :
					page_table(other.page_table),
					free_blocks_head(other.free_blocks_head),
					runtime_1(other.runtime_1),
					runtime_2(other.runtime_2),
					pid(other.pid),
					mem_handle(mem_handle) {}

	Process::Process(Process&& other) :
					free_blocks_head(std::move(other.free_blocks_head)),
					page_table(std::move(other.page_table)),
					mem_handle(std::exchange(other.mem_handle, nullptr)),
					pid(std::exchange(other.pid, ZERO)),
					runtime_1(std::exchange(other.runtime_1, ZERO)),
					runtime_2(std::exchange(other.runtime_2, ZERO)) {}

	Process::~Process() {
		for (int i = 0; i < this->page_table.size(); i++) {
			mem_handle->delete_page(i, this);
		}
		this->page_table.clear();
		while (this->free_blocks_head) {
			auto next = free_blocks_head->next;
			delete free_blocks_head;
			free_blocks_head = next;
		}
	}
}