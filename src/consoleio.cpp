#include "consoleio.h"
#include <iostream>

namespace ziffman::Computer::Utils {
    void print(const std::string& format, ...) {
        va_list args;
        va_start(args, format);
        char* str = new char[200];
        sprintf(str, format.c_str(), args);
        std::cout << str;
        delete[] str;
        va_end(args);
    }
}