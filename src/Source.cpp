#include "Logger.h"

int main(int argc, char** argv) {
	using namespace ziffman::Computer;
	Logger::LogInfo("Hello world!");
	Logger::LogWarn("Did you turn off the lights before leaving ?");
	Logger::LogError("Couldn't find file!");
	Logger::LogDebug("Entered main()");
	Logger::WriteLogs("log.txt");
	return 0;
}