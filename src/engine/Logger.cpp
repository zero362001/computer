#include "engine/Logger.h"
#include "consoleio.h"
#include "stringutils.h"
#include <fstream>

namespace ziffman::Computer::Engine {

#define RED "\e[1;31m"
#define GREEN "\e[1;32m"
#define YELLOW "\e[1;33m"
#define WHITE "\e[1;37m"
#define RESET "\e[0m"

    using ziffman::Computer::Utils::print;

    Logger* Logger::GetInstance() {
        if (instance = nullptr)
            instance = new Logger();
        return instance;
    }

    void Logger::Log(Level level, const std::string& message) {
        switch (level)
        {
        case Level::WARN:
            LogWarn(message);
            break;
        case Level::DEBUG:
            LogDebug(message);
            break;
        case Level::ERROR:
            LogError(message);
            break;
        case Level::INFO:
            LogInfo(message);
        default:
            break;
        }
    }

    void Logger::LogError(const std::string& message) {
        print(std::string(RED) + "Error: " + RESET + message + "\n");
        logs.push_back("Error: " + message + "\n");
    }

    void Logger::LogInfo(const std::string& message) {
        print(std::string(WHITE) + "Message: " + RESET + message + "\n");
        logs.push_back("Message: " + message + "\n");
    }

    void Logger::LogDebug(const std::string& message) {
        print(std::string(WHITE) + "Debug: " + RESET + message + "\n");
        logs.push_back("Debug: " + message + "\n");
    }

    void Logger::LogWarn(const std::string& message) {
        print(std::string(YELLOW) + "Warning: " + RESET + message + "\n");
        logs.push_back("Warning: " + message + "\n");
    }

    void Logger::WriteLogs() {
        std::ofstream stream(path);
        for (auto& str : logs) {
            stream << str;
        }
        stream.close();
    }


}
