#include "stringutils.h"
#include <stdarg.h>

namespace ziffman::Computer::Utils {
    std::string format(const std::string& fmt, ...) {
        // TODO: Convert incoming std::string to C string
        va_list list;
        char* string;
        sprintf(string, fmt.c_str(), list);
        va_end(list);
        return std::string(string);
    }
}