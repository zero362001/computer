#include "Memory.h"
#include "Exception.h"

namespace zifmann::Computer {
	inline bool is_zero(const std::vector<word>& page_table) {
		for (auto& page : page_table) {
			if (page != ZERO) return false;
		} return true;
	}

	bool Memory::initialize() {
		// set all frames (other than the 0 page) as free to be used
		for (word i = 1; i < frame_count; i++) {
			free_frames.push_back(i * frame_size);
		}
		return true;
	}

	word Memory::get_physical_addr(word logical, const Process* process) {
		// first 8 bytes represent the page number, f = page_no
		word f = logical >> frame_count;
		// the next 8 bytes represent the offset, d = offset
		word d = logical & (FRAMESIZE - 1);
		if (f > process->page_table.size()) {
			// TODO: throw an exception
			return ZERO;
		}
		else return process->page_table[f] + d;
	}

	byte Memory::read(word addr, const Process* process) {
		return data[get_physical_addr(addr, process)];
	}

	bool Memory::write(byte data[], word addr, byte n, const Process* process) {
		word physical = get_physical_addr(addr, process);
		for (byte i = physical; i < n; i++) {
			this->data[i] = this->data[physical - i];
		}
		return true;
	}

	word Memory::allocate_frame() {
		if (free_frames.empty()) return ZERO;
		else {
			word frame = free_frames.front();
			free_frames.pop_front();
			return frame;
		}
	}

	word Memory::allocate_bytes(word bytes, Process* process) {
		word addr = ZERO;
		MemoryBlock* iter = process->free_blocks_head;

		// Check if there are any pages allocated for the process, or that
		// there is enough free memory on whatever pages (if any) for that program
		// if not, allocate a new page and register the available memory
		// on that page

		if ((process->page_table.empty()) || is_zero(process->page_table) || (iter == nullptr)) {
			process->page_table.clear(); // in case all the existing pages point to invalid frames (cleared)
			word page = allocate_page(process);
		}
		// Allocating a new page and adding the newly available blocks to the free blocks list
		if ((process->page_table.size() == 0) || is_zero(process->page_table) || (iter == nullptr)) {
			process->page_table.clear();
			word frame = allocate_frame();
			if (frame == ZERO) {
				// TODO: Throw error
				Exception e = Exception{ "Out of frames!", E_OUT_OF_FRAMES };
				return ZERO;
			}
			process->page_table.push_back(frame);
			if (iter) {
				while (iter != nullptr) {
					if (iter->next)
						iter = iter->next;
				}
				iter->size = iter->size + frame_size;
			}
			else iter = new MemoryBlock{ 0, page_size, nullptr };
		}
		iter = process->free_blocks_head;
		MemoryBlock* prev = nullptr, * next = nullptr;
		// Allocating bytes and removing them from the free blocks list
		while (iter != nullptr) {
			next = iter->next;
			if (iter->size == bytes) {
				if (prev) prev->next = iter->next;
				addr = iter->mem;
				delete iter;
				break;
			}
			else if (iter->size > bytes) {
				// if memory block is bigger than required, split it into two blocks
				// one of the size of the required block and one into the size of the
				// remaining blocks and set the 'mem' to the start of the new block and
				// reset the size to the number of the remaining bytes
				addr = iter->mem;
				iter->mem = iter->mem + bytes;
				iter->size -= bytes;
			}
			prev = iter;
			iter = next;
		}
	}

	word Memory::allocate_page(Process* process)
	{
		word frame = allocate_frame(); // request a new frame on physical memory
		if (frame == ZERO) {
			// If no frame available, return ZERO and TODO: add some error/interrupt handling
			return ZERO;
		} // else proceed to add the new frame to the page table
		if (process->page_table.empty()) {
			process->page_table.push_back(frame);
			// add the newly available blocks to free blocks list
			MemoryBlock* block = new MemoryBlock{ 0, page_size, nullptr };
			// return the page number [1st (0) for an empty page table]
			return 0;
		}
		else {
			for (word i = 0; i < process->page_table.size(); i++) {
				if (process->page_table[i] == ZERO) {
					process->page_table[i] = frame;
					// add the newly available blocks to free blocks list
					MemoryBlock* block = new MemoryBlock{ i, page_size, nullptr };
					return i;
				}
			}
			process->page_table.push_back(frame);
			// add the newly available blocks to free blocks list
			MemoryBlock* iter = process->free_blocks_head;
			while (iter->next != nullptr) {
				iter = iter->next;
			}
			MemoryBlock* block = new MemoryBlock{ static_cast<word>(process->page_table.size() - 1), page_size, nullptr };
			iter->next = block;
			return process->page_table.size() - 1;
		}
	}

	void Memory::delete_page(word page, Process* process) {
		// add the currently alloted frame for the page to the free frames list
		// for future use by other processes
		free_frames.push_back(process->page_table[page]);
		process->page_table[page] = ZERO; // reset the page to correspond to the zero page (reserved)

		// start removing blocks in the free blocks list of the processs that were previously on the allocated page
		MemoryBlock* iter = process->free_blocks_head;
		MemoryBlock* prev = nullptr, * next = nullptr;
		word frame_start = process->page_table[page];
		word frame_end = frame_start + frame_size;

		// loop through all the free blocks in the free blocks list
		// blocks may have a maximum size of (frame_size - block_start_offset) bytes such that no block crosses pages
		// this makes managing those blocks much easier at the time of cleanup
		while (iter != nullptr) {
			next = iter->next;
			if (iter->mem >= frame_start && iter->mem < frame_end) {
				if ((iter->mem + iter->size) >= frame_end) {
					iter->mem = frame_end;
					iter->size = ((iter->mem + iter->size) - frame_end);
				}
				else {
					if (prev != nullptr) {
						prev->next = iter->next;
						delete iter;
					}
				}
			}
			prev = iter;
			iter = next;
		}
	}

	void Memory::delete_bytes(word addr, byte n, const Process* process) {
		word page_no = addr >> frame_count;
		word offset = addr & (frame_size - 1);
		byte free_mem_size = n;
		MemoryBlock* iter = process->free_blocks_head, * prev = nullptr;
		while (iter) {
			// check if one of the free blocks is located right before the allocated block
			// and add the allocated block size (to be freed) to the available free block
			word iter_page_no = iter->mem >> frame_count;
			word iter_offset = iter->mem & (frame_size - 1);
			if (iter_page_no == page_no) { // on the same page
				if ((iter_offset + iter->size) == offset) { // ends right before the currently allocated block to be freed
					// add the size of the allocated block to the free block
					iter->size += n;
					return;
				}
			}
			prev = iter;
			iter = iter->next;
		}
		// if it couldn't find any such free blocks in the absolute vicinity, 
		// create a new free block and push it back to the list of free blocks
		MemoryBlock* free_block = new MemoryBlock{ addr, n, nullptr };
		prev->next = free_block;
	}
}